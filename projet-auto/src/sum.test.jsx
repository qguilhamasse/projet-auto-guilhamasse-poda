import { expect, test } from "vitest"
//import {sum} from "./sum.jsx"

/**
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns a + b
 */
function sum(a, b) {
    return (
      a+ b
    ) 
}

/**
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns a - b
 */
function subtract(a, b) {
    return a - b;
  }
  
/**
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns a * b
 */
function multiply(a, b) {
    return a * b;
}
  
/**
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns a / b
 */
function divide(a, b) {
    if (b === 0) {
      throw new Error("Cannot divide by zero");
    }
    return a / b;
}

//TEST

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3)
  })

  test('subtracts 5 - 3 to equal 2', () => {
    expect(subtract(5, 3)).toBe(2);
  });
  
  test('multiplies 4 * 6 to equal 24', () => {
    expect(multiply(4, 6)).toBe(24);
  });
  
  test('divides 8 / 2 to equal 4', () => {
    expect(divide(8, 2)).toBe(4);
  });
  
  test('throws error when dividing by zero', () => {
    expect(() => divide(10, 0)).toThrowError("Cannot divide by zero");
  });